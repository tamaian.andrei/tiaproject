var chartValues = {"0": 0, "1": 0, "2": 0, "3": 0, "5": 0, "8": 0, "12": 0, "21": 0, "144": 0,}


function sendQuestion() {
    var question = document.getElementById("question").value;
    localStorage.setItem('managerQuestion', question)
    // if(!localStorage.getItem("storageChartValues"))
        localStorage.setItem("storageChartValues", JSON.stringify(chartValues))
}

function receiveQuestion() {
    var regeivedQuestion = localStorage.getItem('managerQuestion');
    document.getElementById("managerQuestion").innerHTML = regeivedQuestion;
}

function sendAnswer() {
    var answer = document.getElementById("radioButton").value; // 0,1,2,3,5,8,13,21,144
    console.log("Answer: " + answer)
    var value = JSON.parse(localStorage.getItem("storageChartValues"));
    value[answer + ""] = value[answer + ""] + 1;
    localStorage.setItem("storageChartValues", JSON.stringify(value));
}

function refreshChart() {
    // location.reload();
    var chartValue = JSON.parse(localStorage.getItem("storageChartValues"));
    console.log("chartValues: " + Object.values(chartValue))
    var xValues = Array.from(Object.keys(chartValue)); //Nine values
    console.log("xValues: " + xValues)
    var yValues = Array.from(Object.values(chartValue));
    console.log("yValues: " + yValues)
    var barColors = [
        "#b91d47",
        "#00aba9",
        "#2b5797",
        "#e8c3b9",
        "#1e7145",
        "#E9967A",
        "#D8BFD8",
        "#FFD700",
        "#DA70D6"
    ];

    new Chart("myChart", {
        type: "pie",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            title: {
                display: true,
                text: "Story points chart"
            }
        }
    });
}











